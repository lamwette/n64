<?php 
require "vendor/autoload.php";

ORM::configure('sqlite:data.sqlite3');

//creation d'editeur dans la base
function getOrCreateEditeur($nom_editeur)
{
    $editeur = Model::factory('Editeur')->where('nom_editeur', $nom_editeur)->find_one();

    if($editeur){
        return $editeur;
    }

    $editeur = Model::factory('Editeur')->create();
    $editeur->nom_editeur = $nom_editeur;
    $editeur->save();

    return $editeur;
}

//creation de developpeur dans la base
function getOrCreateDeveloppeur($nom_developpeur)
{
    $developpeur = Model::factory('Developpeur')->where('nom_developpeur', $nom_developpeur)->find_one();

    if($developpeur)
    {
        return $developpeur;
    }
    $developpeur = Model::factory('Developpeur')->create();
    $developpeur->nom_developpeur = $nom_developpeur;
    $developpeur->save();
    return $developpeur;
}

//Creation de genre dans la base
function getOrCreateGenre($nom_genre)
{
    $genre = Model::factory('Genre')->where('nom_genre', $nom_genre)->find_one();

    if($genre)
    {
        return $genre;
    }
    $genre = Model::factory('Genre')->create();
    $genre->nom_genre = $nom_genre;
    $genre->save();
    return $genre;
}

//Import des jeux dans la table de jeux
function importGames($filename)
{
    $csv_file = fopen($filename, 'r');

    ini_set('auto_detect_line_endings',TRUE);
    while ( ($data = fgetcsv($csv_file, 0, ";") ) !== FALSE ) {
        $game = Model::factory('Jeux')->create();
        $editeur = getOrCreateEditeur($data[1]);
        $developpeur = getOrCreateDeveloppeur($data[2]);
        $genre = getOrCreateGenre($data[3]);

        $game->nom = $data[0];
        $game->date = $data[4];
        $game->id_editeur = $editeur->id;
        $game->id_developpeur = $developpeur->id_developpeur;
        $game->id_genre = $genre->id_genre;
        $game->image = $data[5];

        $game->save();
    }
    ini_set('auto_detect_line_endings',FALSE);
}

importGames("base_n64_v1.csv");