<?php 
require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');

$twigConfig = array
(
    
    'debug' => true,
);

//Integration de Twig dans Flight
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) 
{
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    
    //Mon filtre twig transformant le html en markdown
    $twig->addFilter(new Twig_Filter('markdown', function($string)
    {
        return renderHTMLFromMarkdown($string);
    }));

});

//Ouverture sql lite 3
Flight::before('start', function($params, $output){
ORM::configure('sqlite:data.sqlite3');
});

//utiliser render au lieu de view()->display()
Flight::map('render', function($template, $data=array())
{
    Flight::view()->display($template, $data);
});

//Route vers les jeux 
Flight::route('/n64', function()
{
    
    $data = 
    [
        'game' => Jeux::find_many(),
    ];

    Flight::render('dinos.twig', $data);
});


//Route d'ajout en base de données
Flight::route('/n64/ajout/', function(){
    ///var_dump(Flight::request());/
    $data = [
        'game' => Jeux::find_many(),
        'editeurs' => Model::factory('Editeur')->find_many(),
        'developpeurs' => Model::factory('Developpeur')->find_many(),
        'genres' => Model::factory('Genre')->find_many(),
    ];
    Flight::view()->display('ajout.twig', $data);
});

//Methode $_POST
Flight::route('/n64/ajout/add', function(){

   
    if (Flight::request()->method == 'POST'){
        $ajout = Model::factory('Jeux')->create();
        $ajout->nom = Flight::request()->data->nom;
        $ajout->date = Flight::request()->data->date;
        $ajout->id_editeur = Flight::request()->data->editeur;
        $ajout->id_developpeur = Flight::request()->data->developpeur;
        $ajout->id_genre = Flight::request()->data->genre;
        $ajout->save();
    }
    Flight::redirect('/n64/ajout');
});

Flight::route('/n64/@name', function($name)
{
   // var_dump(Flight::request());
    $data = 
    [
        'game' => Model::factory('Jeux')->where('nom', $name)->find_one(),
        
    ];
    Flight::render('onedino.twig', $data);
});


Flight::start();