<?php 
require "vendor/autoload.php";
/*use Michelf\Markdown;
// convertion du HTML en markdown
function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}*/

//Recupération de la table jeux
class Jeux extends Model 
{
    public static $_table = 'jeux';

    public function image_b64()
    {
        return sprintf("data:image/jpeg;base64,%s", base64_encode($this->image));
    }

    public function editeur()
    {
        return $this->belongs_to('Editeur', 'id_editeur')->find_one();
    }
    public function developpeur()
    {
        return $this->belongs_to('Developpeur', 'id_developpeur')->find_one();
    }
    public function genre()
    {
        return $this->belongs_to('Genre', 'id_genre')->find_one();
    }
}

//recuperation table editeur
class Editeur extends Model
{
    public static $_table = 'editeur';

    public function jeux()
    {
        $this->has_many('Jeux', 'id_editeur')->find_many();
    }

}

//recuperation de la table developpeur
class Developpeur extends Model
{
    public static $_table = 'developpeur';
    public static $_id_column = 'id_developpeur';

    public function jeux()
    {
        $this->has_many('Jeux', 'id_developpeur')->find_many();
    }
}

//recuperation de la table genre
class Genre extends Model
{
    public static $_table = 'genre';
    public static $_id_column = 'id_genre';

    public function jeux()
    {
        $this->has_many('Jeux', 'id_genre')->find_many();
    }
}

function openFile($filepath)
{
    return file_get_contents($filepath);
}


