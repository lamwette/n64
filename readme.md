# ![](assets/img/logo.png)

Un simple site web pour répertorier les dinosaures de Jurassic Park via l'utilisation de:

- [Composer](https://getcomposer.org/),
- [FlightPHP](http://flightphp.com/), 
- [Twig](https://twig.symfony.com/),
- [Requests](http://requests.ryanmccue.info/),
- [PHPUnit](https://phpunit.de/),
- et bien d'autres choses .....

## How to ?

1. composer install
2. vendor/bin/phpunit tests/
3. 127.0.0.1:5000
4. Et go sur [127.0.0.1:8000](http://127.0.0.1:8000/)

## Tests ?

La couverture est minimal mais suffisante pour utiliser les tests d'intégrations avec PHPUnit. 

Pour les lancer un simple: vendor/bin/phpunit tests/

## Requirements

mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown


## Purpose

This project is only for information purpose. It's actually a basic application for generating a dummy website.

## Authors

> [Corentin GENIN-LOMIER](https://gitlab.com/lamwette/jurassic-park)
